#AwayAMP
AwayAMP 是什么？
    AwayAMP是一个Apache、PHP、MySQL的集成环境，她可以帮助您快速的部署PHP的开发环境及生产环境。


AwayAMP的特点
    AwayAMP整合了最新的Apache、PHP、MySQL
    AwayAMP可以方便的切换各个版本的Apache或者PHP或者MySQL
    AwayAMP可以通过图形界面快速的设置常用的配置
    AwayAMP目前带有软件版本：Apache(2.2/2.4) PHP(5.2/5.3/5.4/5.5) MySQL(5.0/5.1/5.5)
