unit functions;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, dateutils, IniFiles, UTF8Process, process, dialogs;

const PHPConf = 'LoadModule php5_module "../../php/{PHP_VERSION}/php5apache{APACHE_VERSION}.dll"'#13#10+
'<IfModule mod_php5.c>'#13#10+
'  PHPINIDir "../../php/{PHP_VERSION}/"'#13#10+
'  AddType application/x-httpd-php .php'#13#10+
'  AddType application/x-httpd-php-source .phps'#13#10+
'</IfModule>'#13#10;                               // Apache的php配置文件
const PHP52Conf = 'LoadFile ../../php/5.2/libmysql.dll'#13#10+
'LoadFile ../../php/5.2/libmcrypt.dll'#13#10+
'LoadFile ../../php/5.2/libmhash.dll'#13#10;   // Apache的php配置文件针对php5.2的设置


type
  // 程序状态记录
  TAppStatus = record
    name: string;
    version: string;
    status: Byte;
  end;

procedure Log(msg: String);        // 日志
procedure LoadData;                // 读取配置文件
function RunCMD(CMD: String):Boolean;      // 运行命令
function RunCMDInDir(CMD, Dir: String):Boolean;
procedure ChangeProgramVersion(PID: Byte);   // 更改程序版本,Apache,PHP

var
  ApacheStatus, PHPStatus, MySQLStatus: TAppStatus;  // 服务状态
  LoadingDataStatus: Boolean;  // 载入数据状态

implementation
uses MainForm;

procedure Log(msg: String);
var
  LogFile: Text;
  FileName: String;
begin
  msg := FormatDateTime('[YYYY-mm-dd HH:mm:ss]', Now)+msg;
  FileName := ExtractFilePath(ParamStr(0)) + '..\logs\' + 'controller.log';
  Assign(LogFile, FileName);
  try
    if FileExists(FileName) then
      Append(LogFile)
    else
      Rewrite(LogFile);
    writeln(LogFile, msg);
  finally
    Close(LogFile);
  end;
end;

procedure LoadData;
var
  ConfFile: TIniFile;
  FileName: String;
begin
  // 读取模块信息
  ApacheStatus.name := 'Apache';
  PHPStatus.name := 'PHP';
  MySQLStatus.name := 'MySQL';
  FileName := ExtractFilePath(ParamStr(0)) + '..\conf\' + 'awayamp.ini';
  ConfFile := TIniFile.Create(FileName);
  ApacheStatus.version:=ConfFile.ReadString('apache','version','2.2_VC9');
  PHPStatus.version:=ConfFile.ReadString('php','version','5.3');
  MySQLStatus.version:=ConfFile.ReadString('mysql','version','5.1');

  ConfFile.Free;
end;

function RunCMD(CMD: String): Boolean;
var
  PRun : TProcessUTF8;
begin
  Result := True;
  PRun := TProcessUTF8.Create(nil);
  PRun.CommandLine := CMD;
  PRun.Options := [poWaitOnExit];
  try
    PRun.Execute;
  except
    Result := False;
  end;
  PRun.Free;
end;

function RunCMDInDir(CMD, Dir: String): Boolean;
var
  PRun : TProcessUTF8;
begin
  Result := True;
  PRun := TProcessUTF8.Create(nil);
  PRun.CommandLine := CMD;
  PRun.CurrentDirectory:=Dir;
  PRun.Options := [poWaitOnExit];
  try
    PRun.Execute;
  except
    Result := False;
  end;
  PRun.Free;
end;

procedure ChangeProgramVersion(PID: Byte);
var
  ConfFile: TIniFile;
  FileName: String;
  PHP_VERSION, APACHE_VERSION, PHPConfStr: String;
  TSL: TStringList;
begin
  // Apache版本变更
  if PID = 0 then
  begin
    if (PHPStatus.version = '5.5') and (ApacheStatus.version <> '2.4_VC11') then
    begin
      MessageDlg('提示', 'PHP版本与当前选中版本不兼容，PHP版本更改为5.4',mtInformation,[mbOK], 0);
      PHPStatus.version := '5.4';
      Main.PHP.cbPHPVersion.ItemIndex := Main.PHP.cbPHPVersion.Items.IndexOf(PHPStatus.version);
    end;
  end;

  // PHP版本变更
  if PID = 1 then
  begin
    if (PHPStatus.version = '5.5') and (ApacheStatus.version <> '2.4_VC11') then
    begin
      MessageDlg('提示', 'Apache版本与当前选中版本不兼容，Apache版本更改为2.4_VC11',mtInformation,[mbOK], 0);
      ApacheStatus.version := '2.4_VC11';
      Main.Apache.cbApacheVersion.ItemIndex := Main.Apache.cbApacheVersion.Items.IndexOf(ApacheStatus.version);
    end;
  end;

  // 写入Apache的PHP配置文件
  if (PID = 0) or (PID = 1) then
  begin
    PHP_VERSION := PHPStatus.version;
    if ApacheStatus.version = '2.2_VC9' then APACHE_VERSION := '2_2';
    if (ApacheStatus.version = '2.4_VC9') or (ApacheStatus.version = '2.4_VC11')
      then APACHE_VERSION := '2_4';

    PHPConfStr := '';
    if PHP_VERSION = '5.2' then
      PHPConfStr := PHP52Conf;
    PHPConfStr := PHPConfStr + PHPConf;

    PHPConfStr := StringReplace(PHPConfStr, '{PHP_VERSION}', PHP_VERSION, [rfReplaceAll]);
    PHPConfStr := StringReplace(PHPConfStr, '{APACHE_VERSION}', APACHE_VERSION, [rfReplaceAll]);

    // 写入Apache PHP配置文件
    FileName := ExtractFilePath(ParamStr(0)) + '..\conf\apache\php.conf';
    TSL := TStringList.Create;
    TSL.Text := PHPConfStr;
    TSL.SaveToFile(FileName);
    TSL.Free;
  end;

  // 写入程序配置文件
  FileName := ExtractFilePath(ParamStr(0)) + '..\conf\awayamp.ini';
  ConfFile := TIniFile.Create(FileName);
  ConfFile.WriteString('apache', 'version', ApacheStatus.version);
  ConfFile.WriteString('php', 'version', PHPStatus.version);
  ConfFile.WriteString('mysql', 'version', MySQLStatus.version);
  ConfFile.Free;
end;

initialization
  LoadingDataStatus := True;
  LoadData;
end.

